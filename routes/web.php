<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Auth Routes
Auth::routes();

// Ticket CRUD Routes
Route::get('ticket/create', 'TicketController@create')->name('ticket_create');
Route::post('ticket/save', 'TicketController@save')->name('ticket_save');
Route::get('ticket', 'TicketController@index')->name('ticket_index');
Route::get('ticket/{ticket}', 'TicketController@show')->name('ticket_show');
Route::post('ticket/{ticket}/comment/save', 'CommentController@save')->name('comment_save');
// Ticket Assignment Routes
Route::put('ticket/{ticket}/close', 'TicketController@close')->name('ticket_close');
Route::put('ticket/{ticket}/claim', 'TicketController@claim')->name('ticket_claim');
Route::put('ticket/{ticket}/free', 'TicketController@free')->name('ticket_free');
Route::put('ticket/{ticket}/escalate', 'TicketController@escalate')->name('ticket_escalate');
Route::put('ticket/{ticket}/deescalate', 'TicketController@deescalate')->name('ticket_deescalate');
Route::put('ticket/{ticket}/delegate', 'TicketController@delegate')->name('ticket_delegate');


// Employee Ticket Routes
//Route::get('ticket/helpdesk', 'TicketController@index_helpdesk')->name('ticket_index_helpdesk');

// Trying Laravel With Models
Route::get('models', 'ModelController@index');
Route::get('models/{id}', 'ModelController@show');


// Example Routes
Route::get('/dit_is_een_test', 'TestController@test');

Route::get('/param/{id}', 'TestController@param');

Route::get('/hello_world', function() {
    return 'Hello World';
});

Route::get('/user', 'UserController@index');

Route::get('user/{id}', function($id) {
    return 'User ' . $id;
});

Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {

});

Route::get('person/{name?}', function ($name = null) {
    return $name;
});

Route::get('person/{name}', function ($name) {
    return $name;
})->where('name', '[A-Za-z]+');



