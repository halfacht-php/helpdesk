<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->date('birthdate');
            $table->string('birthplace');
            $table->string('ethnicity');
            $table->string('nationality');
            $table->string('measurements')->nullable();
            $table->integer('height');
            $table->integer('weight');
            $table->string('hair-color');
            $table->boolean('shaved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
