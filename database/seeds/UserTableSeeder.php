<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_ids = DB::table('roles')->pluck('id', 'name');

        DB::table('users')->insert([
            [
                'name' => 'K. Ontevreden',
                'email' => 'kontevreden@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::KLANT],
                'created_at' => now()
            ],
            [
                'name' => 'R. Praatgraag',
                'email' => 'rpraatgraag@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::KLANT],
                'created_at' => now()
            ],
            [
                'name' => 'Floris H',
                'email' => 'florish@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::LINE1],
                'created_at' => now()
            ],
            [
                'name' => 'Jordy H',
                'email' => 'jordyh@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::LINE1],
                'created_at' => now()
            ],
            [
                'name' => 'Jannes Brink',
                'email' => 'jbrink@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::LINE2],
                'created_at' => now()
            ],
            [
                'name' => 'Barry Leito',
                'email' => 'barryleito@mail.com',
                'password' => Hash::make('secret'),
                'role_id' => $role_ids[Role::LINE2],
                'created_at' => now()
            ],
            [
                'name' => 'A Ytsma',
                'email' => 'aytsma@mail.com',
                'password' => Hash::make('notsecret'),
                'role_id' => $role_ids[Role::ADMIN],
                'created_at' => now()
            ],

        ]);
    }
}