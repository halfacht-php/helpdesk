<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => Role::KLANT,
                'created_at' => now()

            ],
            [
                'name' => Role::LINE1,
                'created_at' => now()
            ],
            [
                'name' => Role::LINE2,
                'created_at' => now()
            ],
            [
                'name' => Role::ADMIN,
                'created_at' => now()
            ]
        ]);
    }
}
