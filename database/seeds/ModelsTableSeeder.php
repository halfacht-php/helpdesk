<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModelsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('models')->insert([
            [
                'name' => 'Shae Summers',
                'birthdate' => '1994-11-24',
                'birthplace' => 'Fort Lauderdale, Florida, U.S.',
                'ethnicity' => 'Caucasian, Asian',
                'nationality' => 'American',
                'measurements' => '32F-24-34',
                'height' => '157',
                'weight' => '45',
                'hair-color' => 'brown',
                'shaved' => '1'
            ],
            [
                'name' => 'Dillon Harper',
                'birthdate' => '1991-09-27',
                'birthplace' => 'Jupiter, Florida, USA',
                'ethnicity' => 'Caucasian',
                'nationality' => 'American',
                'measurements' => '34-24-36',
                'height' => '165',
                'weight' => '52',
                'hair-color' => 'brown',
                'shaved' => '1'
            ]
        ]);
    }
}