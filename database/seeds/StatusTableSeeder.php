<?php

use App\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'name' => Status::UNASSIGNED,
                'description' => 'Ticket is aangemaakt en staat in de wachtrij om te worden toegewezen aan een medewerker',
                'rank' => 1,
                'created_at' => now()
            ],
            [
                'name' => Status::LINE1_ASSIGNED,
                'description' => 'Ticket is toegewezen aan een medewerker',
                'rank' => 2,
                'created_at' => now()
            ],
            [
                'name' => Status::ESCALATED,
                'description' => 'Ticket is doorgeschoven naar de tweedelijn',
                'rank' => 3,
                'created_at' => now()
            ],
            [
                'name' => Status::LINE2_ASSIGNED,
                'description' => 'Ticket is toegewezen aan een tweedelijnsmedewerker',
                'rank' => 4,
                'created_at' => now()
            ],
            [
                'name' => Status::FINISH,
                'description' => 'Ticket is afgehandeld',
                'rank' => 0,
                'created_at' => now()
            ]
        ]);
    }
}
