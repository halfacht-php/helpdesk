<?php

namespace App\Http\Controllers;


use App\Role;
use App\Status;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Returns the index View
     */
    public function index()
    {
        if (Auth::user()->can('create', Ticket::class)) {
            // Return view for Customers
            return $this->index_customers();
        }
        elseif (Auth::user()->can('assign', Ticket::class)) {
            // Return view for Employees
            return $this->index_helpdesk();
        }
        else {
            // Return 403
            return View('errors/403');
        }
    }

    /**
     * Returns the view for customers
     */
    private function index_customers()
    {
        $tickets = Auth::user()->submitted_tickets()->orderBy('created_at', 'DESC')->get();

        return View('ticket/index')->with('tickets', $tickets);
    }

    /**
     * Returns the view for Employees
     */
    private function index_helpdesk()
    {
        switch(Auth::user()->role->name) {
            case Role::LINE1:
                $status_name = Status::UNASSIGNED;
                break;
            case Role::LINE2:
                $status_name = Status::ESCALATED;
                break;
            default:
                abort(500);

        }

        // Get Statuses to query Tickets
        $status_unassigned = Status::whereName($status_name)->first();
        $status_closed = Status::whereName(Status::FINISH)->first();

        // Get Tickets assigned to user.
        $tickets = Auth::user()->assigned_tickets()->orderBy('created_at', 'DESC')->get();

        // Get unassigned Tickets by Status
        $unassigned_tickets = $status_unassigned->tickets()->orderBy('created_at', 'DESC')->get();

        // filter closed Tickets
        $assigned_tickets = $tickets->where('status_id', '!=', $status_closed->id);
        $closed_tickets = $tickets->where('status_id', $status_closed->id);

        // Single Queries
//        $assigned_tickets = Auth::user()->assigned_tickets()->where('status_id', '!=', $status_closed->id)->orderBy('created_at', 'DESC')->get();
//        $closed_tickets = $status_closed->tickets()->orderBy('created_at', 'DESC')->get();

        return view('ticket/index_helpdesk', [
            'assigned_tickets' => $assigned_tickets,
            'unassigned_tickets' => $unassigned_tickets,
            'closed_tickets' => $closed_tickets
        ]);
    }

    /**
     * Returns a View to show the Ticket
     */
    public function show(Ticket $ticket)
    {

        $this->authorize('read', $ticket);

        $colleagues = [];

        if (Auth::user()->can('delegate', $ticket)) {
            $colleagues = User::where('role_id', Auth::user()->role_id)
                ->where('id', '!=', Auth::user()->id)
                ->get();
        }

        return View('ticket/show', ['ticket' => $ticket, 'colleagues' => $colleagues]);
    }

    /**
     * Returns a view with a Ticket creation form.
     */
    public function create()
    {
        $this->authorize('create', Ticket::class);
        return View('ticket/create');
    }

    /**
     * Saves a valid ticket or redirects back for an invalid ticket
     */
    public function save(Request $request)
    {

        $this->authorize('create', Ticket::class);

        // Validate
        $request->validate(
            [
                'title' => 'required|max:191',
                'description' => 'required'
            ]
        );

        // Get the status with Eloquent
        $status = Status::whereName(Status::UNASSIGNED)->first();

        // Create new Ticket object
        $ticket = new Ticket();

        // Fill the ticket object with the request values
        $ticket->title = $request->title;
        $ticket->description = $request->description;
        $ticket->status()->associate($status);

        // Save ticket
        $request->user()->submitted_tickets()->save($ticket);

        // redirect to index with success
        return redirect()->route('ticket_index')->with('success', 'Your ticket is saved..');
    }

    // == Ticket Assignment Router Methods ==

    /**
     * Closes the ticket
     */
    public function close(Ticket $ticket) {

        // Authorise if the user may close te Ticket
        $this->authorize('close', $ticket);

        // Get the status object that has the closed status
        $close_status = Status::whereName(Status::FINISH)->first();

        // Associate Ticket with new Status
        $ticket->status()->associate($close_status);

        // Save the Ticket
        $ticket->save();

        // Redirect back
        return redirect()->back()->with('success', __('Ticket has been closed'));

    }

    /**
     * Assigns the Ticket to the user
     */
    public function claim(Ticket $ticket) {

        // Authorise if the user may claim the Ticket
        $this->authorize('claim', $ticket);

        // Get the new status for the Ticket
        switch(Auth::user()->role->name) {
            case Role::LINE1:
                $status = Status::whereName(Status::LINE1_ASSIGNED)->first();
                break;
            case Role::LINE2:
                $status = Status::whereName(Status::LINE2_ASSIGNED)->first();
                break;
            default:
                abort(500);
        }

        // Associate the Ticket with the new Status
        $ticket->status()->associate($status);

        // Save the Ticket
        $ticket->save();

        // Pair the Ticket with the User
        Auth::user()->assigned_tickets()->attach($ticket);

        // Redirect with success
        return redirect()->back()->with('success', __('Ticket has been claimed'));
    }

    /**
     * Free the ticket from the User
     */
    public function free(Ticket $ticket) {

        // Authorise if the user may free the ticket
        $this->authorize('free', $ticket);

        // Get the new Status
        switch(Auth::user()->role->name) {
            case Role::LINE1:
                $status = Status::whereName(Status::UNASSIGNED)->first();
                break;
            case Role::LINE2:
                $status = Status::whereName(Status::ESCALATED)->first();
                break;
            default:
                abort(500);
        }

        // Associate the ticket with the new Status
        $ticket->status()->associate($status);

        // Save the Ticket
        $ticket->save();

        // Remove the Ticket from the User
        Auth::user()->assigned_tickets()->detach($ticket);

        // Redirect with success
        return redirect()->back()->with('success', __('Ticket has been unslaved'));
    }

    /**
     * Escalates the Ticket to Line 2
     */
    public function escalate(Ticket $ticket) {

        // Authorise if the User may Escalate the Ticket
        $this->authorize('escalate', $ticket);

        // Get the new Status
        $status = Status::whereName(Status::ESCALATED)->first();

        // Associate the Ticket with Line 2 Status
        $ticket->status()->associate($status);

        // Save the Ticket
        $ticket->save();

        // Redirect with success
        return redirect()->back()->with('success', __('Ticket has been escalated'));
    }

    /**
     * Deescalates the Ticket back to Line 1
     */
    public function deescalate(Ticket $ticket) {

        // Authorise if the User may Escalate the Ticket
        $this->authorize('deescalate', $ticket);

        // Get the new Status
        $status = Status::whereName(Status::LINE1_ASSIGNED)->first();

        // Associate the Ticket with Line 1 Status
        $ticket->status()->associate($status);

        // Remove the current user form the Ticket
        Auth::user()->assigned_tickets()->detach($ticket);

        // Save the Ticket
        $ticket->save();

        // Redirect with success
        return redirect()->back()->with('success', __('Ticket has been deescalated'));
    }

    /**
     * Delegates the Ticket to another Helpdesk Employee
     */
    public function delegate(Request $request, Ticket $ticket) {

        // Authorise is the user may delegate the Ticket
        $this->authorize('delegate', $ticket);

        // Validate
        $request->validate(
            [
                'colleague_id' => 'required|integer',
            ]
        );

        // Get the new User to assign the Ticket to
        $colleague = User::findOrFail($request->colleague_id);

        // Check if the selected user is a colleague
        if (Auth::user()->role->name != $colleague->role->name) {
            return redirect()->back();
        }

        // Detach current user from Ticket
        Auth::user()->assigned_tickets()->detach($ticket);

        // Attach colleague to Ticket
        $colleague->assigned_tickets()->attach($ticket);

        // Redirect with success
        return redirect()->back()->with('success', 'Ticket has been delegated to ' . $colleague->name);
    }
}
