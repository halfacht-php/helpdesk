<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ModelController extends Controller
{
    public function index() {

        $models = DB::table('models')->select('id', 'name', 'birthdate', 'birthplace', 'ethnicity', 'nationality', 'measurements', 'height', 'weight')->get();

        return view('models.index')->with('models', $models);
    }

    public function show($id) {
        $model = DB::table('models')->where('id', $id)->first();

        return view('models.single')->with('model', $model);
    }
}
