<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test()
    {
        return view('test')->with('db_default', config('database.default'));

    }

    public function param($id)
    {

        return view('testparameter')->with('id', $id);

    }
}
