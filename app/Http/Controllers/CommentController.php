<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Ticket;
use Illuminate\Http\Request;
use Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Save a Comment for a ticket
     */
    public function save(Request $request, $ticket_id)
    {

        // Ticket ophalen
        $ticket = Ticket::findOrFail($ticket_id);

        // Protect with Policy
        $this->authorize('comment', $ticket);

        // Validate
        $validator = Validator::make
        (
            $request->all(),
            [
                'contents' => 'required'
            ]

        );
        // Return to form on fail
        if ($validator->fails()) {
            return redirect()
                ->route('ticket_show', ['id' => $ticket, '#form'])
                ->withErrors($validator)
                ->withInput();
        }

        // New Comment object
        $comment = new Comment();
        $comment->contents = $request->contents;
        $comment->ticket()->associate($ticket);

        // Save
        $request->user()->comments()->save($comment);

        // Redirects to index with success
        return redirect()
            ->route('ticket_show', ['id' => $ticket])
            ->with('success', 'Reactie succesvol verzonden');
    }
}
