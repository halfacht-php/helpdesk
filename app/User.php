<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function submitted_tickets() {
        return $this->hasMany(Ticket::class);
    }

    public function assigned_tickets() {
        return $this->belongsToMany(Ticket::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }


    // == Booleans ==

    public function isCustomer() {
        return $this->role->name == Role::KLANT;
    }

    public function isEmployee() {
        return in_array($this->role->name, [Role::LINE1, Role::LINE2]);
    }

    public function hasSubmittedTicket(Ticket $ticket) {
        return $ticket->user_id === $this->id;
    }

    public function isAssignedToTicket(Ticket $ticket) {
        return $this->assigned_tickets->contains($ticket);
    }


}
