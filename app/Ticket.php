<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

    public function created_by() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status() {
        return$this->belongsTo(Status::class);
    }

    public function assigned_to() {
        return $this->belongsToMany(User::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    // == Booleans ==

    public function isOpen() {
        return $this->status->name != Status::FINISH;
    }
}
