<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    const UNASSIGNED = 'eerstelijn';
    const LINE1_ASSIGNED = 'eerstelijn_toegewezen';
    const ESCALATED = 'tweedelijn';
    const LINE2_ASSIGNED = 'tweedelijn_toegewezen';
    const FINISH = '-weggejorist-';

    public function tickets() {
        return $this->hasMany(Ticket::class);
    }
}
