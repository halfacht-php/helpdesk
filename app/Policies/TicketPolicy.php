<?php

namespace App\Policies;

use App\Role;
use App\Status;
use App\Ticket;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Returns if the user may create a Ticket
     */
    public function create(User $user) {
        // User Role is Customer
        return $user->isCustomer();
    }

    /**
     * Returns if the user may read the Ticket.
     */
    public function read(User $user, Ticket $ticket) {
        // Return true when the user made the ticket or the user in an employee
        return $user->hasSubmittedTicket($ticket) || $user->isEmployee();
    }

    /**
     * Return if the user may have tickets assigned
     */
    public function assign(User $user) {
        // User Role is Line 1 or Line 2
        return $user->isEmployee();
    }

    /**
     * Returns if the user may comment on the Ticket
     */
    public function comment(User $user, Ticket $ticket) {
        // Ticket Status is not Finished, and User submitted the Ticket or is assigned to the Ticket
        return  $ticket->isOpen() && ($user->hasSubmittedTicket($ticket) || $user->isAssignedToTicket($ticket));
    }

    /**
     * Return if the user may close the Ticket
     */
    public function close(User $user, Ticket $ticket) {
        // Ticket Status is not Finished, and User submitted the Ticket or is assigned to the Ticket
        return $ticket->isOpen() && ($user->hasSubmittedTicket($ticket) || $user->isAssignedToTicket($ticket));
    }

    /**
     * Return if the user may claim the Ticket
     */
    public function claim(User $user, Ticket $ticket) {
        // User is from Line 1 and Ticket is unassigned, or User is from Line 2 and Ticket is Escalated.
        return ($user->role->name == Role::LINE1 && $ticket->status->name == Status::UNASSIGNED) ||
            ($user->role->name == Role::LINE2 && $ticket->status->name == Status::ESCALATED);
    }

    /**
     * Returns if the user may free the Ticket
     */
    public function free(User $user, Ticket $ticket) {
        return $user->isAssignedToTicket($ticket) &&
            (($user->role->name == Role::LINE1 && $ticket->status->name == Status::LINE1_ASSIGNED) ||
            ($user->role->name == Role::LINE2 && $ticket->status->name == Status::LINE2_ASSIGNED));
    }

    /**
     * Returns if the user may escalate the Ticket
     */
    public function escalate(User $user, Ticket $ticket) {
        // User is assigned to the Ticket, and Ticket and User are Line 1
        return $user->isAssignedToTicket($ticket) &&
            ($user->role->name == Role::LINE1 && $ticket->status->name == Status::LINE1_ASSIGNED);
    }

    /**
     * Returns if the user may deescalate the Ticket
     */
    public function deescalate(User $user, Ticket $ticket) {
        // User is assigned to the Ticket, and Ticket and User are Line 2
        return $user->isAssignedToTicket($ticket) &&
            ($user->role->name == Role::LINE2 && $ticket->status->name == Status::LINE2_ASSIGNED);
    }

    /**
     * Returns if the user may delegate the Ticket
     */
    public function delegate(User $user, Ticket $ticket) {
        // User is assigned to the Ticket
        return $user->isAssignedToTicket($ticket) &&
            (
                // User is Line 1, The Ticket is Assigned to Line 1 and There are other Line 1 Employees
                (
                    $user->role->name == Role::LINE1 &&
                    $ticket->status->name == Status::LINE1_ASSIGNED &&
                    Role::where('name', Role::LINE1)->first()->users()->count() > 1
                )
                ||
                // User is Line 2, The Ticket is Assigned to Line 2 and There are other Line 2 Employees
                (
                    $user->role->name == Role::LINE2 &&
                    $ticket->status->name == Status::LINE2_ASSIGNED &&
                    Role::where('name', Role::LINE2)->first()->users()->count() > 1
                )
            );
    }

    // == Booleans ==


}
