<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

const KLANT = 'klant';
const LINE1 = 'eerstelijns-medewerker';
const LINE2 = 'tweedelijns-medewerker';
const ADMIN = 'administrator';

    public function users() {
        return $this->hasMany(User::class);
    }
}
