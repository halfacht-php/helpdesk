@extends('layouts.app')

@section('title', 'Tickets')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">

                    <div class="card-header">
                        {{ __('Tickets') }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>

                    <div class="card-body">
                        @forelse ($tickets as $ticket)
                            @include('parts/ticket')
                        @empty
                            {{ __('No tickets available..') }}
                        @endforelse
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
