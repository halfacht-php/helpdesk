@extends('layouts.app')

@section('title', 'Tickets')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-6">
                @include ('parts/session_message')
            </div>
        </div>

        <div class="row justify-content-center">

            @if ($assigned_tickets->isEmpty() && $unassigned_tickets->isEmpty())
                {{--Geen Tickets--}}
                <div class="col-md-6">
                    <div class="card-header">
                        {{ __('Tickets') }}
                    </div>
                    <div class="card-body">
                        {{ __('No tickets available..') }}
                    </div>
                </div>
            @endif

            @if (!$assigned_tickets->isEmpty())
                {{--Assigned Tickets--}}
                <div class="col-md-6">
                    <div class="card-header">
                        {{ __('Assigned Tickets') }}
                    </div>

                    <div class="card-body">
                        @foreach ($assigned_tickets as $ticket)
                            @include('parts/ticket')
                        @endforeach
                    </div>
                </div>

            @endif
            @if (!$unassigned_tickets->isEmpty())

                {{-- Unassigned Tickets --}}
                <div class="col-md-6">
                    <div class="card-header">
                        {{ __('Unassigned Tickets') }}
                    </div>
                    <div class="card-body">
                        @foreach ($unassigned_tickets as $ticket)
                            @include('parts/ticket')
                        @endforeach
                    </div>
                </div>
            @endif
        </div>

        {{--Closed Tickets--}}
        @if (!$closed_tickets->isEmpty())
            <div class="row justify-content-center">
                    <div class="col-md-6">

                        <div class="card-header">
                            {{ __('Closed Tickets') }}
                        </div>

                        <div class="card-body">
                            @foreach ($closed_tickets as $ticket)
                                @include('parts/ticket')
                            @endforeach
                        </div>

                    </div>
            </div>
        @endif
    </div>

@endsection