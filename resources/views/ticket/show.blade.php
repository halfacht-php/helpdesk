@extends('layouts.app')

@section('title', 'Ticket - ' . $ticket->title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @include('parts/session_message')

                <div class="card mb-3">
                    <div class="card-header">
                        <h5 class="card-title float-left">
                            {{ $ticket->title }}
                        </h5>
                        <span class="float-right">
                            {{ $ticket->created_by->name }},
                            <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                        </span>

                    </div>

                    <div class="card-header">
                        {{-- close button --}}
                        @can('close', $ticket)
                            <form class="d-inline-block" action="{{ route('ticket_close', $ticket) }}" method="post">
                                @method('PUT')
                                @csrf
                                <input class="btn btn-danger" type="submit" value="{{ __('close') }}"/>
                            </form>
                        @endcan

                        {{-- claim button --}}
                        @can('claim', $ticket)
                            <form class="d-inline-block" action="{{ route('ticket_claim', $ticket) }}" method="post">
                                @method('PUT')
                                @csrf
                                <input class="btn btn-success" type="submit" value="{{ __('claim') }}"/>
                            </form>
                        @endcan

                        {{-- free button --}}
                        @can('free', $ticket)
                            <form class="d-inline-block" action="{{ route('ticket_free', $ticket) }}" method="post">
                                @method('PUT')
                                @csrf
                                <input class="btn btn-warning" type="submit" value="{{ __('free') }}"/>
                            </form>
                        @endcan

                        {{-- escalate button --}}
                        @can('escalate', $ticket)
                            <form class="d-inline-block" action="{{ route('ticket_escalate', $ticket) }}" method="post">
                                @method('PUT')
                                @csrf
                                <input class="btn btn-info" type="submit" value="{{ __('escalate') }}"/>
                            </form>
                        @endcan

                        {{-- deescalate button --}}
                        @can('deescalate', $ticket)
                            <form class="d-inline-block" action="{{ route('ticket_deescalate', $ticket) }}" method="post">
                                @method('PUT')
                                @csrf
                                <input class="btn btn-info" type="submit" value="{{ __('deescalate') }}"/>
                            </form>
                        @endcan

                        {{-- delegate button --}}
                        @can('delegate', $ticket)
                           <button type="button" class="btn btn-primary"
                                data-toggle="modal" data-target="#delegateModal">
                               {{ __('delegate') }}
                           </button>

                            @include('parts/delegate_modal')
                        @endcan


                    </div>

                    <div class="card-body">
                        {!! nl2br(e($ticket->description)) !!}
                    </div>

                    <div class="card-footer">
                        {{ $ticket->status->description }}
                    </div>
                </div>

                @forelse($ticket->comments as $comment)
                    <div class="card mb-3">
                        <div class="card-header">
                            <h5 class="card-title">
                                {{ $comment->user->name }}
                            </h5>
                        </div>

                        <div class="card-body">
                            {!! nl2br(e($comment->contents)) !!}
                        </div>

                        <div class="card-footer">
                            {{ $comment->created_at->toFormattedDateString() }}
                        </div>
                    </div>
                @empty
                    <div class="card mb-3">
                        <div class="card-header">
                            {{ __('No comments..') }}
                        </div>
                    </div>

                @endforelse

                {{--Create Comment Form--}}
                @can('comment', $ticket)
                    <div class="card">
                        <div class="card-header">{{ __('New Comment') }}</div>

                        <div class="card-body">
                            <form id="form" method="POST" action="{{ route('comment_save', ['id' => $ticket]) }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="contents"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Contents') }}</label>

                                    <div class="col-md-6">
                                    <textarea id="contents"
                                              class="form-control{{ $errors->has('contents') ? ' is-invalid' : '' }}"
                                              name="contents">{{ old('contents') }}</textarea>

                                        @if ($errors->has('contents'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contents') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Add Comment') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endcan {{--End of Create Comment Form--}}
            </div>
        </div>
    </div>
@endsection
