@extends('layouts.app')

@section('title', 'Home')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">


                @if (session('status'))
                    <div class="card">
                        <div class="card-body">

                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        Dit is de Home pagina
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection