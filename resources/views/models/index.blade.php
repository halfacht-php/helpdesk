<?php
/**
 * Created by PhpStorm.
 * User: alwin
 * Date: 19-Nov-18
 * Time: 14:23
 */

?>

@extends('layouts.app')

@section('title', 'Models')

@section('content')

<table>
    <tr>
        @foreach ($models[0] as $column => $value)
            <th>{{ ucfirst($column )}}</th>
        @endforeach
    </tr>
    @foreach ($models as $model)
        <tr>
            @foreach ($model as $key => $value)

                    <td>
                        @if ($key == 'name')
                            <a href="/models/{{ $model->id }}">{{ $value }}</a>
                        @else
                            {{ $value }}
                        @endif
                    </td>
            @endforeach
        </tr>
    @endforeach
</table>

@endsection