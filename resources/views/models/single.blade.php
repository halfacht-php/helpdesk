<?php
/**
 * Created by PhpStorm.
 * User: alwin
 * Date: 19-Nov-18
 * Time: 14:23
 */

?>

@extends('layouts.app')

@section('title', $model->name)

@section('content')



    <h2><a href="/models"><-- Terug naar overzicht</a></h2>

    <h1>{{ $model->name }}</h1>


    <table>
        @foreach ($model as $key => $value)
            <tr>

                <td>
                    {{ ucfirst($key) }}:
                </td>
                <td>
                    {{ $value }}
                </td>
            </tr>
        @endforeach
    </table>

@endsection


