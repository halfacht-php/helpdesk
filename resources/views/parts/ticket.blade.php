<div class="card mb-3">

    @can ('read', $ticket)
        {{-- Header --}}
        <div class="card-header">
            <h5 class="card-title float-left">
                <a href="{{ route('ticket_show', ['id' => $ticket]) }}">
                    {{ $ticket->title }}
                </a>
            </h5>
            <span class="float-right">
                {{ $ticket->created_by->name }},
                <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
            </span>
        </div>

        {{-- Body --}}
        <div class="card-body">
            <p class="card-text">{!! nl2br(e($ticket->description)) !!}</p>
        </div>

        {{-- Footer --}}
        <div class="card-footer">
            {{ $ticket->status->description }}
        </div>
    @else
        {{-- Header --}}
        <div class="card-header">
            <h5 class="card-title float-left">
                {{ $ticket->title }}
            </h5>
            <span class="float-right">
                {{ $ticket->created_by->name }},
                <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
            </span>
        </div>
    @endcan
</div>