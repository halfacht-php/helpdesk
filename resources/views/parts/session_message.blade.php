@if (session('success'))
    <div class="card mb-3">
        <div class="card-header">
            {{ __('Message') }}
        </div>
        <div class="card-body">
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        </div>
    </div>
@endif
