<div class="modal fade" id="delegateModal" tabindex="-1" role="dialog" aria-labelledby="Delegate ticket" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delegate Ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('ticket_delegate', $ticket) }}" method="post">
                    @method('PUT')
                    @csrf
                    <label for="colleague">To: </label>
                    <select id="colleague" name="colleague_id">
                        @foreach ($colleagues as $colleague)
                            <option value="{{ $colleague->id }}">{{ $colleague->name }}</option>
                        @endforeach
                    </select>
                    <input type="submit" value="{{ __('delegate') }}" />
                </form>
            </div>
        </div>
    </div>

</div>