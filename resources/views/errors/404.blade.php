@extends('layouts.app')

@section('title', '404')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="panel-heading">
                    <h1>Onbekend adres</h1>
                </div>
                <div class="panel-body">
                    <p>Dit adres bestaat niet (meer)</p>
                    <a href="{{ url('/') }}">Terug naar Home</a>
                </div>

            </div>
        </div>
    </div>
@endsection
