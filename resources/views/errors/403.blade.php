@extends('layouts.app')

@section('title', '403')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="panel-heading">
                    <h1>Niet toegestaan</h1>
                </div>
                <div class="panel-body">
                    <p>U hebt hier niets te zoeken.</p>
                </div>

            </div>
        </div>
    </div>
@endsection
